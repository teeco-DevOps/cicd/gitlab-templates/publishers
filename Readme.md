# Gitlab Pipline templates

This project contains a file with all templates and needed variables.

## Usage
Choose a job to include in publish stage in order to successfully reach the desired result.

```yaml
stages:
- publish
```
Add needed variables and files inside the Repo and inside the CICD variables in gitlab.
Create .gitlab-ci.yml file and add to the project with the desired stages included.
## Examples:
```yaml
## Publish Stage
### Dotnet 6.0 Artifactory Publish
#### Needs: dotnet_6.0.yml
#### Variables:
    # INTERNAL_ARTIFACTORY_USERNAME
    # INTERNAL_ARTIFACTORY_PASSWORD
    # INTERNAL_ARTIFACTORY_REPO
    # PROJECT
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/publishers'
    ref: master
    file: '/artifactory_dotnet_publish.yml'
### Golang Pack Docker Publish
#### Variables:
    # REGISTRY_URL : url of the registry registry-host.com
    # REGISTRY_USERNAME : username for the registry
    # REGISTRY_PASSWORD  : password for the registry
    # REGISTRY_REPO : repository name for the registry
    # PROJECT_NAME : # usually PROJECT_NAME=$(echo $CI_PROJECT_NAME | tr '[:upper:]' '[:lower:]')
    # REGISTRY_PROJECT : full project name for the registry usually $REGISTRY_REPO/$OPERATOR/$OPERATION/$PROJECT_NAME
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/publishers'
    ref: master
    file: '/golang-pack-registry-publish.yml'
### Java Maven Docker Publish
#### Variables:
    # REGISTRY_URL
    # REGISTRY_USERNAME
    # REGISTRY_PASSWORD
    # REGISTRY_REPO
    # REGISTRY_PROJECT
    # MAVEN_CLI_OPTS
    # MAVEN_TESTS
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/publishers'
    ref: master
    file: '/java-mvn-pack-registry-publish.yml'
### Nodejs Artifactory Publish
#### Needs: nodejs.yml
#### Variables:
    # REGISTRY_URL
    # REGISTRY_USERNAME
    # REGISTRY_PASSWORD
    # REGISTRY_REPO
    # REGISTRY_PROJECT
    # TEMPLATE_NGINX_CONF
    # TEMPLATE_MIME_TYPE
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/publishers'
    ref: master
    file: '/nodejs_nginx_pack_publish.yml'
### Generic Dockerfile Publish
#### Needs: Valid binary build stage with build.env
#### Variables:
    #   REGISTRY_URL
    #   REGISTRY_USERNAME
    #   REGISTRY_PASSWORD
    #   REGISTRY_PROJECT
    #   DOCKERFILE (Path to Dockerfile either a file in the repo or file variable in gitlab)
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/publishers'
    ref: master
    file: '/dockerfile_publish.yml'
```